import { PropsWithChildren } from 'react'
import Header from './header'
import Footer from './footer'
import { chakra, useColorModeValue, Container } from '@chakra-ui/react'

export default function Layout ({ children }: PropsWithChildren<any>) {
  const bg = useColorModeValue('gray.50', 'gray.900')

  return (
    <chakra.div bg={bg}>
      <Header />
        <Container as="main" maxW="5xl">
          {children}
        </Container>
      <Footer />
    </chakra.div>
  )
}
